ServerEvents.tags('item', tags => {
    for(let item of global.removed_items) {
        tags.add("c:hidden_from_recipe_viewers", item);
    }

    for(let item in global.replaced_items) {
        tags.add("c:hidden_from_recipe_viewers", item);
    }
});

ServerEvents.recipes(recipes => {
    for(let item in global.replaced_items) {
        let replacement = global.replaced_items[item];
        recipes.replaceInput({ input: item, not: { mod: "kubejs" } }, item, replacement);
        recipes.replaceOutput({ output: item }, item, replacement);
    }

    for(let item of global.removed_items) {
        recipes.remove({ output: item });
    }
});
