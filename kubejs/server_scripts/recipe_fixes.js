ServerEvents.recipes(recipes => {
    recipes.replaceInput({ input: "minecraft:dirt"},"minecraft:dirt","#minecraft:dirt");
    recipes.replaceInput({ input: "immersive_weathering:ash_layer_block" }, "immersive_weathering:ash_layer_block", "#kubejs:ash");
    recipes.replaceInput({ input: "supplementaries:ash" }, "supplementaries:ash", "#kubejs:ash");
    recipes.replaceInput({ input: "farmersdelight:tree_bark" }, "farmersdelight:tree_bark", "#immersive_weathering:bark");
    recipes.replaceInput({ input: "minecraft:chest"}, "minecraft:chest", "#forge:chests/wooden");

    recipes.shapeless(Item.of("farmersdelight:rope",1), ["quark:rope"]);
    recipes.shapeless(Item.of("farmersdelight:rope",1), ["supplementaries:rope"]);

    recipes.remove({id: "toms_storage:inventory_cable_connector"});

    recipes.shaped(Item.of('toms_storage:ts.inventory_cable_connector',1),
        [
            'MPC',
        ], {
            M: 'minecraft:comparator',
            P: '#minecraft:planks',
            C: 'toms_storage:ts.inventory_cable'
        }
    );
});

ServerEvents.tags('item', tags => {

    for (const item of ["blue_skies:turquoise_dirt","blue_skies:coarse_turquoise_dirt","blue_skies:lunar_dirt",
        "blue_skies:coarse_lunar_dirt","theabyss:dirt","byg:lush_dirt"]) {
        tags.add("minecraft:dirt",item);
    }

    tags.add("forge:bread/sliced", "some_assembly_required:bread_slice");
    tags.add("kubejs:ash", "supplementaries:ash");
    tags.add("kubejs:ash", "immersive_weathering:ash_layer_block");

    tags.add("immersive_weathering:bark","farmersdelight:tree_bark");
})
