global.removed_items = [
    "cold_sweat:waterskin",
    "enigmaticlegacy:enigmatic_elytra",
    "create_things_and_misc:brass_knife",
    "create_things_and_misc:zinc_knife",
    "additional_lights:fire_for_standing_torch_s",
    "additional_lights:fire_for_standing_torch_l",
    "additional_lights:fire_for_fire_pit_s",
    "additional_lights:fire_for_fire_pit_l",
    "additional_lights:soul_fire_for_standing_torch_s",
    "additional_lights:soul_fire_for_standing_torch_l",
    "additional_lights:soul_fire_for_fire_pit_s",
    "additional_lights:soul_fire_for_fire_pit_l",
];

global.replaced_items = {
    "quark:rope" : "farmersdelight:rope",
    "supplementaries:rope" : "farmersdelight:rope",
    "extradelight:bread_slice" : "some_assembly_required:bread_slice",
}
